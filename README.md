Presence Control System
===============================================
@Author: Saif Addin Ellafi

@Email: saif1988@gmail.com

## Challenge
Design and implement the backend for a presence control system for the employees of a
company. You need to take into consideration that the employees must always clock in and
out by registering their fingerprint.

The “People” department will need:
* Access to the employee’s presence and time control.
* Generation of time sheets of effective worked hours.
* Reception of asynchronous notifications regarding: total absence time, absence anomalies
(for example because of on-call duties), etc.

## Proposed Solution
The architecture proposed here is meant to show the key components of a communication system.

I have focused the design key aspects on the implementation of the following components:

* A RESTful web service that handles communication of Employees and Managers. Implemented using `http4s`
* Authentication, even not implemented reflects the following factors:
  * Privacy, HTTP protocols do not carry sensitive information in URI constructs
  * Authentication comes along Authorization of user role (manager or employee)
  * It also takes care of verifying existence of active employees, fingerprint control and timestamp validity
  * Although not implemented, this problems applies for a good API Gateway, since communication between the protocol and the backend service is mostly Async
  * A user cache can also be implemented on this layer if demand is high and scales
* RESTful Async DSL that pushes notifications such as checkin and checkout of employees at a given timestamp
* Async "late" communication with a persistent database
* Event logs tracking of every notification in the system. Even though implemented on the same DBRMS, it is highly suggested to use an event based database such as Elastic 
* An analytics layer would favor dashboard creation based on the event based storage
* An Actor system was also considered, but I favored a simple DSL for the time assigned
* Generic models were given for a relation database, this serves for indexable queries, implemented using `Slick`

![Architecture Overview](Stratio-PCS.png)

## Design choices
* There aren't any strong algorithmic factors in this application, however constant time queries are favored, immutable collections and functional `scala.conccurent.Future` mapping was given priority.
* Scala `cats` is utilized as part of the `http4s` DSL layer, which favored the handling of async calls and push backs to the backend.
* Error handling is kept minimal to simple scenarios such as non-existing users `BadRequest`, unauthorized access `Forbidden` and `Accepted` async notification push
* Implicits and Type Classes were extensively used for type inferation and mostly decoding
* Unit testing is the best way to try the library, since representative `headers` and `POST` messages are constructed to reflect a dummy authentication system

## Package structure
* auth: Authenticator dummy behavioral class that acts as an authentication and verification layer
* db: Slick ORM for long term storage. Package object schemas are provided in idiomatic ways. Foreign keys and auto generators included.
* model: Main actor representation in the architecture.
* server: Handles server connectivity, DSL routing and filtering
* root: Main will start the program. DummyContent will act as a starting point of storage.

## Usage
SBT will be required, with a Java 8 JDK installation. Scala utilized is 2.11.12 and sbt 0.13.18 for familiarity purposes.

`sbt "testOnly org.saif.pcs.db.DatabaseSuite"` will execute database related tests and use cases

`sbt "testOnly org.saif.pcs.server.PresenceServerTestSpec"` will execute life-like use cases

`sbt run` will simply execute the servers and await requests. Not recommended since it will be necessary to follow up header and POST requirements. Unit tests will be easier as a starting point.

## Other Concluding Notes
Time spend on this kind of projects can go forever, there are many things to improve and add.
However, I kept everything as clear and simple as possible. 

`http4s` it self may be too idiomatic and functional,
sometimes causing possible confusions because of the huge amount of implicits in scope.

Glad to accept any kind of feedback.