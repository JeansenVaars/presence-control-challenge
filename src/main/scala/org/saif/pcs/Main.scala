package org.saif.pcs

import java.sql.Timestamp

import cats.effect.{ExitCode, IO, IOApp}
import cats.implicits._
import org.saif.pcs.db.{EmployeeAccessor, EventLogAccessor, ManagerAccessor}
import org.saif.pcs.model.{Employee, EmployeeStatus, EventLog, EventLogType, Manager}
import org.saif.pcs.server.PresenceControlServer

object Main extends IOApp {

  println("Initializing Database...")
  EmployeeAccessor.init()
  ManagerAccessor.init()
  EventLogAccessor.init()
  println("Database initialized.")

  DummyContent.loadDBDefaults()

  def run(args: List[String]): IO[ExitCode] =
    PresenceControlServer.stream[IO].compile.drain.as(ExitCode.Success)

}