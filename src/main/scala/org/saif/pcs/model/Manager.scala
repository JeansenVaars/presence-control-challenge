package org.saif.pcs.model

case class Manager(
                    id: Option[Long],
                    name: String
                 ) {
  override def toString: String = {
    val s = StringBuilder.newBuilder
    s.append(s"id: ${id.get}\n")
    s.append(s"name: $name")
    s.toString()
  }
}
