package org.saif.pcs.model

case class EventLog(
                     id: Option[Long],
                     employeeId: Long,
                     time: java.sql.Timestamp,
                     event: String,
                     endTime: Option[java.sql.Timestamp] = None,
                     description: Option[String] = None
                   )
object EventLogType extends Enumeration {
  type Event = Value
  val CheckIn, CheckOut, ProjectBegin, ProjectEnd, Downtime = Value
}
