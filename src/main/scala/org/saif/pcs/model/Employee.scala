package org.saif.pcs.model

case class Employee(
                     id: Option[Long],
                     name: String,
                     status: EmployeeStatus.Status
                 ) {
  override def toString: String = {
    val s = StringBuilder.newBuilder
    s.append(s"id: ${id.get}\n")
    s.append(s"name: $name\n")
    s.append(s"status: ${status.toString}")
    s.toString()
  }
}

object EmployeeStatus extends Enumeration {
  type Status = Value
  val Active, Inactive = Value

  import slick.jdbc.H2Profile.api._
  implicit val statusMapper = MappedColumnType.base[EmployeeStatus.Status, String](
    e => e.toString,
    s => EmployeeStatus.fromString(s)
  )

  def fromString(s: String): Value = {
    s.toLowerCase match {
      case "active" => Active
      case "inactive" => Inactive
    }
  }
}
