package org.saif.pcs.auth

import org.http4s.Headers
import org.http4s.util.CaseInsensitiveString
import org.saif.pcs.DummyContent

import scala.util.Random

/**
 * This object represents a dummy authentication process
 * It should not be considered meaningful in any way
 * only to represent hypothetical results
 */
object Authenticator {

  case class AuthHeader(userId: Long, fingerprintHash: Option[Long])

  def parseEmployeeHeader(request: Headers): Option[AuthHeader] = {
    val res = request.get(CaseInsensitiveString("some_token")).map(_.value)
    res match {
      case Some("unauthorized") => None
      case Some("not_exists") => Some(AuthHeader(99999L, None))
      case Some("specific") => Some(AuthHeader(request.get(CaseInsensitiveString("this_id")).map(_.value.toLong).get, Some(Random.nextLong())))
      case _ =>
        val selected = DummyContent.getRandomEmployee
        Some(AuthHeader(selected.id.get, Some(Random.nextLong())))
    }

  }
  def parseManagerHeader(request: Headers): Option[AuthHeader] = {
    request.get(CaseInsensitiveString("some_token")).map(_.value) match {
      case Some("unauthorized") => None
      case Some("not_exists") => Some(AuthHeader(99999L, None))
      case _ =>
        val selected = DummyContent.getRandomManager
        Some(AuthHeader(selected.id.get, Some(Random.nextLong())))
    }
  }

  def encodeSession(userId: Long): Long = {
    Random.nextLong()
  }

  def verify(userId: Long): Boolean = {
    true
  }

  def verify(userId: Long, hash: Long): Boolean = {
    true
  }

}
