package org.saif.pcs

import java.sql.Timestamp

import org.saif.pcs.db.{EmployeeAccessor, EventLogAccessor, ManagerAccessor}
import org.saif.pcs.model._

import scala.util.Random

/**
 * This object represents a dummy ingestion process
 * It should not be considered meaningful in any way
 * only to represent hypothetical content
 */
object DummyContent {

  def loadDBDefaults(): Unit = {
    println("Loading default entities...")
    EmployeeAccessor.insertEmployees(Seq(
      Employee(None, "Peter Gordon", EmployeeStatus.Active),
      Employee(None, "Laura Mell", EmployeeStatus.Active),
      Employee(None, "John Peterson", EmployeeStatus.Active),
      Employee(None, "Barry Burton", EmployeeStatus.Active),
      Employee(None, "Dean Godfrey", EmployeeStatus.Inactive)
    ))
    ManagerAccessor.insertManagers(Seq(
      Manager(None, "Admin Admin")
    ))
    println("Database loaded.")
    EventLogAccessor.insertLogs(Seq(
      EventLog(
        None,
        EmployeeAccessor.getEmployeeBlock(3).get.id.get,
        new Timestamp(System.currentTimeMillis()),
        EventLogType.CheckIn.toString,
        None,
        None
      ),
      EventLog(
        None,
        EmployeeAccessor.getEmployeeBlock(3).get.id.get,
        new Timestamp(System.currentTimeMillis()+5000L),
        EventLogType.CheckIn.toString,
        None,
        None
      ),
      EventLog(
        None,
        EmployeeAccessor.getEmployeeBlock(3).get.id.get,
        new Timestamp(System.currentTimeMillis()+20000L),
        EventLogType.Downtime.toString,
        Some(new Timestamp(System.currentTimeMillis()+20000L)),
        Some("Had to pick up children at school.")
      )
    ))
  }

  def getRandomEmployee: Employee = {
    val allEmployees = EmployeeAccessor.getEmployeesBlock().filter(_.status == EmployeeStatus.Active)
    allEmployees.apply(new Random().nextInt(allEmployees.length))
  }

  def getRandomManager: Manager = {
    val allManagers = ManagerAccessor.getManagersBlock()
    allManagers.apply(new Random().nextInt(allManagers.length))
  }

}
