package org.saif.pcs.server

import cats.Applicative
import cats.effect.IO
import org.saif.pcs.auth.Authenticator
import org.saif.pcs.auth.Authenticator.AuthHeader
import org.saif.pcs.db.{EmployeeAccessor, EventLogAccessor}
import org.saif.pcs.model.Employee
import org.saif.pcs.server.EmployeeRequestHandler.Downtime

import scala.util.{Failure, Try}

/** http4s idioms and skeleton for handling requests and responses. In my opinion this is very noisy :-) */

trait EmployeeRequestHandler[F[_]]{
  type SessionToken = Long
  def employeeAuth(employeeId: Long, authHeader: AuthHeader, requireFingerprint: Boolean): Try[(Employee, SessionToken)]
  def logCheckin(employee: Employee, sessionToken: SessionToken): Unit
  def logCheckout(employee: Employee, sessionToken: SessionToken): Unit
  def logDowntime(employee: Employee, sessionToken: SessionToken, downtime: IO[Downtime]): Unit
}

object EmployeeRequestHandler {
  implicit def apply[F[_]](implicit ev: EmployeeRequestHandler[F]): EmployeeRequestHandler[F] = ev

  case class Downtime(begin: Long, end: Long, description: String)

  def impl[F[_]: Applicative]: EmployeeRequestHandler[F] = new EmployeeRequestHandler[F] {

    override def employeeAuth(employeeId: Long, authHeader: AuthHeader, requireFingerprint: Boolean): Try[(Employee, SessionToken)] = {
      Try {
        if (requireFingerprint)
          Authenticator.verify(employeeId, authHeader.fingerprintHash.getOrElse(return Failure(new Exception("Unauthorized"))))
        else
          Authenticator.verify(employeeId)
        (EmployeeAccessor.getEmployeeBlock(employeeId).get, Authenticator.encodeSession(employeeId))
      }
    }

    override def logCheckin(employee: Employee, sessionToken: SessionToken): Unit = {
      EventLogAccessor.checkin(employee, sessionToken)
    }

    override def logCheckout(employee: Employee, sessionToken: SessionToken): Unit = {
      EventLogAccessor.checkout(employee, sessionToken)
    }

    override def logDowntime(employee: Employee, sessionToken: SessionToken, downtime: IO[Downtime]): Unit = {
      downtime.unsafeRunAsync {
        case Left(e) => throw new Exception(e.getMessage)
        case Right(d) =>
          EventLogAccessor.registerDowntime(
            employee,
            sessionToken,
            d.description,
            new java.sql.Timestamp(d.begin),
            new java.sql.Timestamp(d.end)
          )
      }
    }
  }

}