package org.saif.pcs.server

import cats.effect.{IO, Sync}
import org.http4s.HttpRoutes
import org.http4s.dsl.Http4sDsl
import io.circe.generic.auto._
import org.http4s.circe._
import org.saif.pcs.auth.Authenticator
import org.saif.pcs.server.EmployeeRequestHandler.Downtime

import scala.util.{Failure, Success}

object EmployeeDSLRouting {

  def employeeNotifications[F[_] : Sync](H: EmployeeRequestHandler[F]): HttpRoutes[F] = {
    val dsl = new Http4sDsl[F] {}
    import dsl._

    HttpRoutes.of[F] {

      case req @ GET -> Root / "employee" / "checkin" =>
        try {
          /** Fake authentication layer, decode SSL, parse headers, session tokens and cookies */
          Authenticator.parseEmployeeHeader(req.headers) match {
            case Some(authToken) => H.employeeAuth(authToken.userId, authToken, requireFingerprint = true) match {
              case Success((employee, assignedSessionToken)) =>
                H.logCheckin(employee, assignedSessionToken)
                Accepted()
              case Failure(_) => BadRequest()
            }
            case None => Forbidden()
          }

        } catch {
          case e: Throwable => InternalServerError(s"Something went wrong:\n${e.getMessage}")
        }

      case req @ GET -> Root / "employee" / "checkout" =>
        try {
          /** Fake authentication layer, decode SSL, parse headers, session tokens and cookies */
          Authenticator.parseEmployeeHeader(req.headers) match {
            case Some(authToken) => H.employeeAuth(authToken.userId, authToken, requireFingerprint = false) match {
              case Success((employee, assignedSessionToken)) =>
                H.logCheckout(employee, assignedSessionToken)
                Accepted()
              case Failure(_) => BadRequest()
            }
            case None => Forbidden()
          }
        } catch {
          case e: Throwable => InternalServerError(s"Something went wrong:\n${e.getMessage}")
        }

      case req @ POST -> Root / "employee" / "downtime" =>
        try {
          /** Fake authentication layer, decode SSL, parse headers, session tokens and cookies */
          Authenticator.parseEmployeeHeader(req.headers) match {
            case Some(authToken) => H.employeeAuth(authToken.userId, authToken, requireFingerprint = false) match {
              case Success((employee, assignedSessionToken)) =>
                implicit val decoder = jsonOf[IO, Downtime]
                import org.http4s.circe.CirceEntityDecoder._
                val downtime = req.as[Downtime].asInstanceOf[IO[Downtime]]
                H.logDowntime(employee, assignedSessionToken, downtime)
                Accepted()
              case Failure(_) => BadRequest()
            }
            case None => Forbidden()
          }
        } catch {
          case e: Throwable => InternalServerError(s"Something went wrong:\n${e.getMessage}")
        }

    }
  }
}