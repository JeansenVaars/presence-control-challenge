package org.saif.pcs.server

import java.sql.Timestamp

import org.saif.pcs.auth.Authenticator
import org.saif.pcs.db.{EmployeeAccessor, EventLogAccessor, ManagerAccessor}
import org.saif.pcs.model.{EventLog, Manager}
import cats.Applicative
import io.circe.literal._
import io.circe.Encoder
import org.http4s.EntityEncoder
import org.http4s.circe._
import org.saif.pcs.server.ManagerRequestHandler.QueryResponse

import scala.util.Try

trait ManagerRequestHandler[IO[_]] {

  def managerAuth(managerId: Long): Try[Manager]
  def query(employeeId: Long): QueryResponse
  def queryByTime(employeeId: Long, timeBegin: java.sql.Timestamp, timeEnd: java.sql.Timestamp): QueryResponse

}

object ManagerRequestHandler {

  implicit def apply[IO[_]](implicit ev: ManagerRequestHandler[IO]): ManagerRequestHandler[IO] = ev

  case class QueryResponse(begin: Timestamp, end: Timestamp, description: String, employeeName: String, events: List[EventLog])

  implicit val queryEncoder: Encoder[QueryResponse] = Encoder.instance { query: QueryResponse =>
    json"""{
            "name": ${query.employeeName},
            "begin": ${query.begin.getTime.toString},
            "end": ${query.end.getTime.toString},
            "description": ${query.description},
            "events": ${query.events}
            }"""
  }

  implicit val eventLogEncoder: Encoder[EventLog] = Encoder.instance { eventLog: EventLog =>
    json"""{
            "name": ${eventLog.event.toString},
            "time": ${eventLog.time.getTime.toString},
            "endTime": ${eventLog.endTime.map(_.getTime.toString).getOrElse("null")},
            "description": ${eventLog.description.getOrElse("null")}
            }"""
  }

  implicit def responseQueryEncoder[F[_] : Applicative]: EntityEncoder[F, QueryResponse] =
    jsonEncoderOf[F, QueryResponse]

  implicit def responseEventLogEncoder[F[_] : Applicative]: EntityEncoder[F, List[EventLog]] =
    jsonEncoderOf[F, List[EventLog]]

  def impl[IO[_]: Applicative]: ManagerRequestHandler[IO] = new ManagerRequestHandler[IO] {
    override def managerAuth(managerId: Long): Try[Manager] = {
      Try {
        Authenticator.verify(managerId)
        ManagerAccessor.getManagerBlock(managerId).get
      }
    }

    override def query(employeeId: Long): QueryResponse = {
      val name = EmployeeAccessor.getEmployeeBlock(employeeId).get.name
      val events = EventLogAccessor.logsByEmployeeId(employeeId)
      val time = new Timestamp(System.currentTimeMillis())
      QueryResponse(time, time, "Full Employee Query", name, events.toList)
    }

    override def queryByTime(employeeId: Long, timeBegin: java.sql.Timestamp, timeEnd: java.sql.Timestamp): QueryResponse = {
      val name = EmployeeAccessor.getEmployeeBlock(employeeId).get.name
      val events = EventLogAccessor.logsByEmployeeIdByTime(employeeId, timeBegin, timeEnd)
      val time = new Timestamp(System.currentTimeMillis())
      QueryResponse(timeBegin, timeEnd, "Full Employee in range", name, events.toList)
    }
  }

}
