package org.saif.pcs.server

import cats.effect.Sync
import org.http4s.HttpRoutes
import org.http4s.dsl.Http4sDsl
import org.saif.pcs.auth.Authenticator

import scala.util.{Failure, Success}

object ManagerDSLRouting {

  def managerRequests[IO[_] : Sync](H: ManagerRequestHandler[IO]): HttpRoutes[IO] = {
    val dsl = new Http4sDsl[IO] {}
    import dsl._
    HttpRoutes.of[IO] {

      case req @ GET -> Root / "manager" / "query" / employeeId =>
        try {
          /** Fake authentication layer, decode SSL, parse headers, session tokens and cookies */
          Authenticator.parseManagerHeader(req.headers) match {
            case Some(authToken) => H.managerAuth(authToken.userId) match {
              case Success(_) => {
                try {
                  val resp = H.query(employeeId.toLong)
                  Ok(resp)
                } catch {
                  case e: NumberFormatException => BadRequest()
                }
              }
              case Failure(_) => BadRequest()
            }
            case None => Forbidden()
          }
        } catch {
          case e: Throwable => InternalServerError(s"Something went wrong:\n${e.getMessage}")
        }

      case req @ GET -> Root / "manager" / "query" / employeeId / timeBegin / timeEnd =>
        try {
          /** Fake authentication layer, decode SSL, parse headers, session tokens and cookies */
          Authenticator.parseManagerHeader(req.headers) match {
            case Some(authToken) => H.managerAuth(authToken.userId) match {
              case Success(_) => {
                try {
                  val resp = H.queryByTime(employeeId.toLong, new java.sql.Timestamp(timeBegin.toLong), new java.sql.Timestamp(timeEnd.toLong))
                  Ok(resp)
                } catch {
                  case e: NumberFormatException => BadRequest()
                }
              }
              case Failure(_) => BadRequest()
            }
            case None => Forbidden()
          }

        } catch {
          case e: Throwable => InternalServerError(s"Something went wrong:\n${e.getMessage}")
        }
    }
  }
}
