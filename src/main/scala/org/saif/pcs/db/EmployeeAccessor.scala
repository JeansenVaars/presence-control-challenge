package org.saif.pcs.db

import org.saif.pcs.model.{Employee, EmployeeStatus}
import slick.jdbc.H2Profile.api._
import slick.jdbc.meta.MTable

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

object EmployeeAccessor extends PresenceControlDB {

  import EmployeeStatus._

  def getTables: Future[Vector[MTable]] = db.run(MTable.getTables)

  def insertEmployees(newEmployees: Seq[Employee]): Future[_] =
    db.run(employees ++= newEmployees)

  def insertEmployeesBlock(newEmployees: Seq[Employee], waitAtMost: Duration = 60 seconds): Unit =
    Await.result(insertEmployees(newEmployees), waitAtMost)

  def getEmployeesBlock(status: EmployeeStatus.Status = EmployeeStatus.Active, waitAtMost: Duration = 60 seconds): Seq[Employee] = {
    val task = db.run(employees.filter(_.status === status).result)

    Await.result(task, waitAtMost)
  }

  def countEmployeesBlock(waitAtMost: Duration = 60 seconds): Int = {
    val task = db.run(employees.length.result)

    Await.result(task, 60 seconds)
  }

  def getEmployeeBlock(employeeId: Long, status: EmployeeStatus.Status = EmployeeStatus.Active): Option[Employee] = {
    val query = employees.filter(e => {
      e.id === employeeId && e.status === status
    }).result.headOption
    Await.result(db.run(query), 60 seconds)
  }

}
