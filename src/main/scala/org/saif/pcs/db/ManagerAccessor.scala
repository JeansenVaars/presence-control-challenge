package org.saif.pcs.db

import org.saif.pcs.model.Manager
import slick.jdbc.meta.MTable

import slick.jdbc.H2Profile.api._

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

object ManagerAccessor extends PresenceControlDB {
  def getTables: Future[Vector[MTable]] = db.run(MTable.getTables)

  def insertManagers(newManagers: Seq[Manager]): Future[_] =
    db.run(managers ++= newManagers)

  def insertManagerBlock(newManagers: Seq[Manager], waitAtMost: Duration = 60 seconds): Unit =
    Await.result(insertManagers(newManagers), waitAtMost)

  def getManagersBlock(waitAtMost: Duration = 60 seconds): Seq[Manager] = {
    val task = db.run(managers.result)
    Await.result(task, waitAtMost)
  }

  def getManagerBlock(managerId: Long): Option[Manager] = {
    val query = managers.filter(e => {
      e.id === managerId
    }).result.headOption
    Await.result(db.run(query), 60 seconds)
  }

  def countManagersBlock(waitAtMost: Duration = 60 seconds): Int = {
    val task = db.run(managers.length.result)

    Await.result(task, 60 seconds)
  }
}
