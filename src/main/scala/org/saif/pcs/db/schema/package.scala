package org.saif.pcs.db

import org.saif.pcs.model.{Employee, EmployeeStatus, EventLog, Manager}
import slick.jdbc.H2Profile.api._
import slick.lifted.ForeignKeyQuery

/**
  * Add schema layouts here
  */
package object schema {

  class Employees(tag: Tag)
    extends Table[Employee](tag, "EMPLOYEES") {

    def id: Rep[Long] = column[Long]("EMPLOYEE_ID", O.PrimaryKey, O.AutoInc)
    def name: Rep[String] = column[String]("NAME")
    def status: Rep[EmployeeStatus.Status] = column[EmployeeStatus.Status]("STATUS")

    def * = (id.?, name, status).mapTo[Employee]
  }

  protected val employees = TableQuery[Employees]

  class Managers(tag: Tag)
    extends Table[Manager](tag, "MANAGERS") {

    def id: Rep[Long] = column[Long]("MANAGER_ID", O.PrimaryKey, O.AutoInc)
    def name: Rep[String] = column[String]("NAME")

    def * = (id.?, name).mapTo[Manager]
  }

  class EventLogs(tag: Tag)
    extends Table[EventLog](tag, "EVENT_LOGS") {

    def id: Rep[Long] = column[Long]("EVENT_ID", O.PrimaryKey, O.AutoInc)
    def employeeId: Rep[Long] = column[Long]("EMPLOYEE_ID")
    def employee: ForeignKeyQuery[Employees, Employee] = foreignKey(
      name = "EMPLOYEE_FK",
      employeeId,
      employees
    )(
      _.id,
      onUpdate = ForeignKeyAction.Restrict,
      onDelete = ForeignKeyAction.Restrict
    )
    def time: Rep[java.sql.Timestamp] = column[java.sql.Timestamp]("TIME")
    def event: Rep[String] = column[String]("EVENT")
    def endTime: Rep[Option[java.sql.Timestamp]] = column[Option[java.sql.Timestamp]]("END_TIME")
    def description: Rep[Option[String]] = column[Option[String]]("DESCRIPTION")

    def * = (id.?, employeeId, time, event, endTime, description).mapTo[EventLog]

  }

}
