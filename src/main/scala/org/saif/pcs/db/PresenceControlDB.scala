package org.saif.pcs.db

import org.saif.pcs.db.schema.{Employees, EventLogs, Managers}
import slick.jdbc.H2Profile.api._

import scala.concurrent.Await
import scala.concurrent.duration._

trait PresenceControlDB extends AutoCloseable {

  protected final val employees: TableQuery[Employees] = TableQuery[Employees]
  protected final val managers: TableQuery[Managers] = TableQuery[Managers]
  protected final val eventLogs: TableQuery[EventLogs] = TableQuery[EventLogs]

  @transient
  protected final def db: Database = {
    PresenceControlDB.idb.getOrElse(throw new IllegalStateException("DB Must be initialized first. Call init()"))
  }

  /** NOT thread safe */
  final def init(dbName: String = "h2mem") {

    if (PresenceControlDB.idb.isEmpty) {

      PresenceControlDB.idb = Some(Database.forConfig(dbName))

      val task = PresenceControlDB.idb.get.run((
        employees.schema ++
          managers.schema ++
          eventLogs.schema
        ).create
      )
      Await.result(task, Duration.Inf)

    }
  }

  override def close(): Unit = PresenceControlDB.idb.foreach(_.close())

}

object PresenceControlDB {

  protected final var idb: Option[Database] = None

}
