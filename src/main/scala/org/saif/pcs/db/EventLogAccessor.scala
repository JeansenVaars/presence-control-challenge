package org.saif.pcs.db

import java.sql.Timestamp

import org.saif.pcs.model.{Employee, EventLog, EventLogType}
import slick.jdbc.H2Profile.api._
import slick.jdbc.meta.MTable

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

object EventLogAccessor extends PresenceControlDB {

  def getTables: Future[Vector[MTable]] = db.run(MTable.getTables)

  def insertLogs(logs: Seq[EventLog], waitAtMost: Duration = 60 seconds): Unit = {
    val task = db.run(eventLogs ++= logs)
    Await.result(task, waitAtMost)
  }

  def getEventLogs(waitAtMost: Duration = 60 seconds): Seq[EventLog] = {
    val task = db.run(eventLogs.result)
    Await.result(task, waitAtMost)
  }

  def checkin(employee: Employee, sessionToken: Long): Future[Int] = {
    db.run(
      eventLogs += EventLog(
        None,
        employee.id.get,
        new Timestamp(System.currentTimeMillis()),
        EventLogType.CheckIn.toString
        )
    )
  }

  def checkout(employee: Employee, sessionToken: Long): Future[Int] = {
    db.run(
      eventLogs += EventLog(
        None,
        employee.id.get,
        new Timestamp(System.currentTimeMillis()),
        EventLogType.CheckOut.toString
      )
    )
  }

  def registerDowntime(employee: Employee, sessionToken: Long, description: String, begin: java.sql.Timestamp, end: java.sql.Timestamp): Future[Int] = {
    db.run(
      eventLogs += EventLog(
        None,
        employee.id.get,
        begin,
        EventLogType.Downtime.toString,
        Some(end),
        Some(description)
      )
    )
  }

  def logsByEmployeeId(employeeId: Long, waitAtMost: Duration = 60 seconds): Seq[EventLog] = {
    val task = db.run(
      eventLogs.filter(_.employeeId === employeeId).sortBy(_.time).result
    )
    Await.result(task, 60 seconds)
  }

  def logsByEmployeeIdByTime(employeeId: Long, begin: java.sql.Timestamp, end: java.sql.Timestamp, waitAtMost: Duration = 60 seconds): Seq[EventLog] = {
    val task = db.run(
      eventLogs.filter(e =>
        e.employeeId === employeeId &&
        e.time >= begin &&
        e.time <= end
      ).sortBy(_.time).result
    )
    Await.result(task, 60 seconds)
  }

}
