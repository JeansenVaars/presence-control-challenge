package org.saif.pcs.db

import java.io.File

import org.saif.pcs.DummyContent
import org.scalatest._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Seconds, Span}

import scala.io.Source

class DatabaseSuite extends FunSuite with BeforeAndAfterAll with ScalaFutures {

  implicit override val patienceConfig = PatienceConfig(timeout = Span(5, Seconds))

  override def beforeAll() {
    /** Do not remove, this ensures unique local context */
    EmployeeAccessor.init()
    ManagerAccessor.init()
    EventLogAccessor.init()
    DummyContent.loadDBDefaults()
  }

  test("Create virtual database Schemas") {
    val tables = EmployeeAccessor.getTables.futureValue

    assert(tables.size == 3)
    assert(tables.count(_.name.name.equalsIgnoreCase("EMPLOYEES")) == 1)
    assert(tables.count(_.name.name.equalsIgnoreCase("MANAGERS")) == 1)
    assert(tables.count(_.name.name.equalsIgnoreCase("EVENT_LOGS")) == 1)

  }

  test("Get Employees in virtual database") {
    val loaded = EmployeeAccessor.getEmployeesBlock()
    assert(loaded.length == 4)
    info(s"Loaded ${loaded.length} employees")
  }

  test("Get Managers in virtual database") {
    val loaded = ManagerAccessor.getManagersBlock()
    assert(loaded.length == 1)
    info(s"Loaded ${loaded.length} managers. Count is ${ManagerAccessor.countManagersBlock()}")
  }

  test("Get Event Logs in virtual database") {
    val loaded = EventLogAccessor.getEventLogs()
    assert(loaded.length == 3)
    info(s"Loaded ${loaded.length} logs.")
  }

  override def afterAll() {
    EmployeeAccessor.close()
    ManagerAccessor.close()
    EventLogAccessor.close()
  }
}
