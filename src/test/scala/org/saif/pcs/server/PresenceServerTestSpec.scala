package org.saif.pcs.server

import cats.effect.IO
import org.saif.pcs.db.{EmployeeAccessor, EventLogAccessor, ManagerAccessor}
import cats.implicits._
import io.circe.syntax._
import org.http4s._
import org.http4s.circe._
import org.http4s.implicits._
import org.saif.pcs.DummyContent
import org.scalatest.{BeforeAndAfterAll, FunSuite}
import org.scalatest.concurrent.ScalaFutures
import org.saif.pcs.server.EmployeeRequestHandler.Downtime

class PresenceServerTestSpec extends FunSuite with BeforeAndAfterAll with ScalaFutures {

  /** Do not remove, this ensures unique local context */
  override def beforeAll() {
    EmployeeAccessor.init()
    ManagerAccessor.init()
    EventLogAccessor.init()
    DummyContent.loadDBDefaults()
  }

  def check[A](actual:        IO[Response[IO]],
               expectedStatus: Status,
               expectedBody:   Option[A])(
                implicit ev: EntityDecoder[IO, A]
              ): Unit =  {
    val actualResp         = actual.unsafeRunSync
    val statusCheck        = actualResp.status == expectedStatus
    val bodyCheck          = expectedBody.fold[Boolean](
      actualResp.body.compile.toVector.unsafeRunSync.isEmpty)( // Verify Response's body is empty.
      expected => actualResp.as[A].unsafeRunSync == expected
    )

    assert(bodyCheck, s"because response body:\n${actualResp.as[String].unsafeRunSync()}" +
      s" is not expected body:\n${expectedBody.getOrElse("")}")

    assert(statusCheck, s"becase response status" +
      s" <${actualResp.status}> is not expected status <$expectedStatus>")
  }

  def checkStatusPrintBody[A](
                               actual: IO[Response[IO]],
                               expectedStatus: Status,
                               description: String
                             )(implicit ev: EntityDecoder[IO, A]): Unit =  {
    val actualResp = actual.unsafeRunSync
    val statusCheck = actualResp.status == expectedStatus

    println("==================================================")
    println(s"Request: $description\n\nResponse body:\n${actualResp.as[String].unsafeRunSync()}")
    println("==================================================")

    assert(statusCheck, s"becase response status" +
      s" <${actualResp.status}> is not expected status <$expectedStatus>")
  }

  test("Employee REST testing") {

    val dsl = EmployeeDSLRouting.employeeNotifications(EmployeeRequestHandler.impl[IO])

    val checkinValid: IO[Response[IO]] =
      dsl.orNotFound.run(
        Request(
          method = Method.GET,
          uri = uri"/employee/checkin",
          headers = Headers.of(
            Header("some_token", "specific"),
            Header("this_id", "1")
          )
        )
      )
    check(checkinValid, Status.Accepted, None)

    val checkinInvalidUser: IO[Response[IO]] =
      dsl.orNotFound.run(
        Request(
          method = Method.GET,
          uri = uri"/employee/checkin",
          headers = Headers.of(Header("some_token", "not_exists"))
        )
      )
    check(checkinInvalidUser, Status.BadRequest, None)

    val checkinUnauthorizedUser: IO[Response[IO]] =
      dsl.orNotFound.run(
        Request(
          method = Method.GET,
          uri = uri"/employee/checkin",
          headers = Headers.of(Header("some_token", "unauthorized"))
        )
      )
    check(checkinUnauthorizedUser, Status.Forbidden, None)

    val checkoutValid: IO[Response[IO]] =
      dsl.orNotFound.run(
        Request(method = Method.GET, uri = uri"/employee/checkout", headers = Headers.of(Header("some_token", "valid")))
      )
    check(checkoutValid, Status.Accepted, None)

//    val downtimeJson = Json.obj(
//      ("begin", Json.fromLong(1565657383916L)),
//      ("end",  Json.fromLong(1565657383916L+1000L)),
//      ("description",  Json.fromString("Got to pick up the kids."))
//    )

    import io.circe.generic.auto._
    val downtimeJson = Downtime(1565657383916L, 1565657383916L+1000L, "Got to pick up the kids.").asJson

    val downtime: IO[Response[IO]] =
      dsl.orNotFound.run(
        Request(
          method = Method.POST,
          uri = uri"/employee/downtime",
          headers = Headers.of(
            Header("some_token", "specific"),
            Header("this_id", "1")
          )
        ).withEntity(downtimeJson)
      )
    check(downtime, Status.Accepted, None)
  }

  test("Manager REST testing") {

    val dslm = ManagerDSLRouting.managerRequests(ManagerRequestHandler.impl[IO])

    val employee = EmployeeAccessor.getEmployeeBlock(1L).get

    val queryValidUri = s"/manager/query/${employee.id.get}"
    val queryValid: IO[Response[IO]] = {
      dslm.orNotFound.run(
        Request(
          method = Method.GET,
          uri = Uri.fromString(queryValidUri).toOption.get,
          headers = Headers.of(Header("some_token", "valid"))
        )
      )
    }
    checkStatusPrintBody(queryValid, Status.Ok, s"Valid auth query to $queryValidUri")

  }

  override def afterAll() {
    EmployeeAccessor.close()
    ManagerAccessor.close()
    EventLogAccessor.close()
  }

}