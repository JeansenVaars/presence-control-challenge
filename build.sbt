val Http4sVersion = "0.20.9"
val CirceVersion = "0.11.1"
val Specs2Version = "4.1.0"
val LogbackVersion = "1.2.3"

lazy val root = (project in file("."))
  .settings(
    organization := "org.saif.pcs",
    name := "presence-control-system",
    version := "0.0.1-SNAPSHOT",
    scalaVersion := "2.11.12",
    libraryDependencies ++= http4sDependencies ++ dbDependencies,
    addCompilerPlugin("org.typelevel" %% "kind-projector"     % "0.10.3"),
    addCompilerPlugin("com.olegpy"    %% "better-monadic-for" % "0.3.0")
  )

lazy val http4sDependencies = Seq(
  "org.http4s"      %% "http4s-blaze-server" % Http4sVersion,
  "org.http4s"      %% "http4s-blaze-client" % Http4sVersion,
  "org.http4s"      %% "http4s-circe"        % Http4sVersion,
  "org.http4s"      %% "http4s-dsl"          % Http4sVersion,
  "io.circe"        %% "circe-core"          % CirceVersion,
  "io.circe"        %% "circe-generic"       % CirceVersion,
  "org.specs2"      %% "specs2-core"         % Specs2Version % "test",
  "ch.qos.logback"  %  "logback-classic"     % LogbackVersion,
  // Optional for auto-derivation of JSON codecs
  "io.circe" %% "circe-generic" % CirceVersion,
  // Optional for string interpolation to JSON model
  "io.circe" %% "circe-literal" % CirceVersion
)

lazy val dbDependencies = Seq(
  "com.typesafe.slick" %% "slick" % "3.3.1",
  "org.slf4j" % "slf4j-nop" % "1.7.10",
  "com.h2database" % "h2" % "1.4.187",
  "org.scalatest" %% "scalatest" % "2.2.4" % "test"
)

scalacOptions ++= Seq(
  "-deprecation",
  "-encoding", "UTF-8",
  "-language:higherKinds",
  "-language:postfixOps",
  "-feature",
  "-Ypartial-unification",
  "-Xfatal-warnings",
  "-language:implicitConversions"
)

fork in run := true

testOptions in Test += Tests.Argument("-oD")

mainClass in run := Some("org.saif.pcs.Main")